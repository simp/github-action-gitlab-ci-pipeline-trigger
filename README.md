# github-action-gitlab-ci-pipeline-trigger
Live Dangerously and TRIGGER GITLAB CI FROM GITHUB ACTIONS (srsly: see disclaimers)

[![Verify Action](https://github.com/simp/github-action-gitlab-ci-pipeline-trigger/workflows/Verify%20Action/badge.svg)](https://github.com/simp/github-action-gitlab-ci-pipeline-trigger/actions?query=workflow%3A%22Verify+Action%22)
<br>
<br>
[![tag badge](https://img.shields.io/github/v/tag/simp/github-action-gitlab-ci-pipeline-trigger)](https://github.com/simp/github-action-gitlab-ci-pipeline-trigger/tags)
[![license badge](https://img.shields.io/github/license/simp/github-action-gitlab-ci-pipeline-trigger)](./LICENSE)
[![size badge](https://img.shields.io/github/size/simp/github-action-gitlab-ci-pipeline-trigger/dist/index.js)](./dist)

* A detailed description of what the action does
* Required input and output arguments
* Optional input and output arguments
* Secrets the action uses
* Environment variables the action uses
* An example of how to use your action in a workflow


